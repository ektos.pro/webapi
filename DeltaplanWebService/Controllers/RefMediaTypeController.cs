﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using DeltaplanWebService.BusinessLogic;
using DeltaplanWebService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DeltaplanWebService.Controllers
{
    public class RefMediaTypeController : Controller
    {
        private DpMainContext _dpMainContext;
        private int _itemsOnPageByConfig;
        public RefMediaTypeController(DpMainContext dpMainContext, IConfiguration configuration)
        {
            _dpMainContext = dpMainContext;
            _itemsOnPageByConfig = Convert.ToInt32(configuration.GetSection("ItemsOnPageByDefault").Value);
        }
        [HttpGet]
        [FormatFilter]
        public IEnumerable<RefMediaType> Get(int? page = null, int? itemsOnPage = null)
        {
            IQueryable<RefMediaType> result;
            var filterDictionary = Filtration.GetFilterDictionary(typeof(DtBase), Request);

            if (filterDictionary.Count > 0)
                result = _dpMainContext.RefMediaType.Where(filterDictionary.PredicateStringBuilder(),
                    filterDictionary.Values.ToArray());
            else
                result = _dpMainContext.RefMediaType;

            if (page == null || page.Value < 1)
                return result;
            var items = itemsOnPage ?? _itemsOnPageByConfig;
            return result.Page(page.Value, items);
        }
    }
}