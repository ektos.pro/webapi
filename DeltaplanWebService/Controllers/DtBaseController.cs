﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using DeltaplanWebService.BusinessLogic;
using DeltaplanWebService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeltaplanWebService.Controllers
{    
    [Route("api/[controller]")]
    [ApiController]
    public class DtBaseController : Controller
    {
        private DpMainContext _dpMainContext;
        private int _itemsOnPageByConfig;
        public DtBaseController(DpMainContext dpMainContext, IConfiguration configuration)
        {
            _dpMainContext = dpMainContext;
            _itemsOnPageByConfig = Convert.ToInt32(configuration.GetSection("ItemsOnPageByDefault").Value);
        }

        [HttpGet]
        [FormatFilter]
        public IEnumerable<DtBase> Get(int? page = null, int? itemsOnPage = null)
        {
            IQueryable<DtBase> result;
            var filterDictionary = Filtration.GetFilterDictionary(typeof(DtBase), Request);

            if (filterDictionary.Count > 0)
                result = _dpMainContext.DtBase.Where(filterDictionary.PredicateStringBuilder(),
                    filterDictionary.Values.ToArray());
            else
                result = _dpMainContext.DtBase;

            if (page == null || page.Value < 1)
                return result;
            var items = itemsOnPage ?? _itemsOnPageByConfig;
            return result.Page(page.Value, items);
        }

    }
}
