﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using DeltaplanWebService.BusinessLogic;
using DeltaplanWebService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DeltaplanWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DtTechController : Controller
    {
        private DpMainContext _dpMainContext;
        private int _itemsOnPageByConfig;
        public DtTechController(DpMainContext dpMainContext, IConfiguration configuration)
        {
            _dpMainContext = dpMainContext;
            _itemsOnPageByConfig = Convert.ToInt32(configuration.GetSection("ItemsOnPageByDefault").Value);
        }
        [HttpGet]
        [FormatFilter]
        public IEnumerable<DtTech> Get(int? page = null, int? itemsOnPage = null)
        {
            IQueryable<DtTech> result;
            var filterDictionary = Filtration.GetFilterDictionary(typeof(DtBase), Request);

            if (filterDictionary.Count > 0)
                result = _dpMainContext.DtTech.Where(filterDictionary.PredicateStringBuilder(),
                    filterDictionary.Values.ToArray());
            else
                result = _dpMainContext.DtTech;

            if (page == null || page.Value < 1)
                return result;
            var items = itemsOnPage ?? _itemsOnPageByConfig;
            return result.Page(page.Value, items);
        }
    }
}
