﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace DeltaplanWebService.BusinessLogic
{
    public class Filtration
    {
        public static Dictionary<object, object> GetFilterDictionary(Type modelType, HttpRequest request)
        {
            var properties = modelType.GetProperties().Select(x => x.Name).ToArray();
            var filterDictionary = new Dictionary<object, object>();
            foreach (var param in request.Query)
                if (properties.Contains(param.Key))
                    filterDictionary.Add(param.Key, param.Value.ToString());
            return filterDictionary;
        }
    }
}
