﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeltaplanWebService
{
    public static class WebServiceExtensions
    {
        public static string PredicateStringBuilder(this Dictionary<object, object> dictionary)
        {
            if (dictionary == null)
                return null;
            if (dictionary.Count.Equals(0))
                return null;
            var sb = new StringBuilder();
            for (int i = 0; i < dictionary.Count; i++)
            {
                sb.Append($"{dictionary.ElementAt(i).Key} = @{i} and ");
            }
            sb.Remove(sb.Length - 5, 5);
            return sb.ToString();
        }

    }
}
