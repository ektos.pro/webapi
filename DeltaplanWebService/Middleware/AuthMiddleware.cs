﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DeltaplanWebService
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;
        public AuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, DpMainContext dpMainContext)
        {
            var isExist = false;
            var headerKey = context.Request.Headers.FirstOrDefault(x => x.Key.Equals("API_KEY")).Value;
            var headerRequstKey = context.Request.Query.FirstOrDefault(x => x.Key.Equals("API_KEY")).Value;
                if (dpMainContext.StmApiClients.Any(x => (x.ApiKey.Equals(headerKey) || x.ApiKey.Equals(headerRequstKey))
                    && (!x.ExpirationDate.HasValue || x.ExpirationDate > DateTime.Now)))
                    isExist = true;
            if (isExist)
                await _next.Invoke(context);
            else
                await context.Response.WriteAsync("Access deny");
        }


    }
}
