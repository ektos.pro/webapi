﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeltaplanWebService.Model
{
    public class StmApiClients
    {
        [Key]
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ApiKey { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
